import {useSelector,useDispatch} from "react-redux"
import {actions} from "./components/counter/counterSlice"

import "./App.css"

function App() {
    const counter = useSelector((state)=> state.count)
    const reachedMax = useSelector((state)=> state.reachedMaxVal)

    const dispatch = useDispatch();

    const updateMinValue = (event)=>{
      dispatch(actions.minValue(event.target.value))
    }

    const updateMaxValue = (event)=>{
      dispatch(actions.maxValue(event.target.value))
    }

    const incCounter = () =>{
        dispatch(actions.increment())
    }

    const decCounter = ()=>{
      dispatch(actions.decrement())
    }

    const  resetCounter = ()=>{
      dispatch(actions.reset())
    }


  return (
    <div className="main-container">
        <h1>{counter}</h1>
        {reachedMax && <h5>Max value reached</h5>}
        <div className="steps-max-container">
          <h2>Steps</h2>
          <div className="min-steps-container">
            <button value={5} onClick={updateMinValue} className="steps-max-buttons">5</button>
            <button value={10} onClick={updateMinValue} className="steps-max-buttons">10</button>
            <button value={15} onClick={updateMinValue} className="steps-max-buttons">15</button>
          </div>
        </div>
        <div>
          <h2>Max Value</h2>
          <div className="max-steps-container">
            <button value={15} onClick={updateMaxValue} className="steps-max-buttons">15</button>
            <button value={100} onClick={updateMaxValue} className="steps-max-buttons">100</button>
            <button value={200} onClick={updateMaxValue} className="steps-max-buttons">200</button>
          </div>
        </div>
        <div className="buttons-container">
            <button onClick={incCounter} className="func-buttons" type="button">Increment</button>
            <button onClick={decCounter} className="func-buttons" type="button">Decrement</button>
            <button onClick={resetCounter} className="func-buttons" type="button">Reset</button>
        </div>
    </div>
  )
}

export default App