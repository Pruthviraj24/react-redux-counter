import {createSlice} from "@reduxjs/toolkit"
import {configureStore} from "@reduxjs/toolkit"

const initialState = {
    count:0,
    maxVal:200,
    minVal:1,
    reachedMaxVal:false
}

export const counterSlice = createSlice({
    name:"counter",
    initialState,
    reducers:{
        increment(state){
            if((Number(state.count)+Number(state.minVal)) >= Number(state.maxVal) ){
                state.reachedMaxVal = true
                state.count = Number(state.maxVal)
            }else{
                state.count += Number(state.minVal)
            }
            
        },
        decrement(state){
            if(Number(state.count) <= Number(state.maxVal) ){
                state.count -= Number(state.minVal)
                state.reachedMaxVal = false
            }
            
        },
        reset(state){
            state.count = 0
            state.reachedMaxVal = false
        },
        maxValue(state,action){
            state.maxVal = action.payload
        },
        minValue(state,action){
            state.minVal = action.payload
        }
    }
})

export const actions = counterSlice.actions;

export const store  = configureStore({
    reducer:counterSlice.reducer
})

